clear all
[file path] = uigetfile('*mp3');
[y,fs]=mp3read([path file]);  
t = 10;         %badany czas, optymalnie t=10                
cz = 50   ;     %ilosc czesci podzia�u sekundy
d=fs/cz;        %szerokosc ramki rms
y= y';          %by y bylo wlasciwe
[qqq rozm] = size(y);
PODZ = [0.35:0.05:0.70];
podz = PODZ(floor(rand()*8)+1);     %wybieranie losowego punktu podzialu w rozsadnych miejscach utworu

y = y(1,rozm*podz:rozm*podz+44100*t-1) + y(2,rozm*podz:rozm*podz+44100*t-1) ;
clear qqq;                  %juz niepotrzebne zmienne
clear rozm;
figure (1);               %ewentualne rysowanie wybranego przedzia�u


plot (y);
title(['Tytu�: ' file ]);
xlim = ([0 fs*t]);
ylim([-1,1]);
% wavplay(y,fs);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%liczenie RMS dla 9 zakres�w spektrum
RMS =[0 0 0 0 0 0 0 0 0];           %by uzyska� odpowiedni rozmiar, pozniej to bedzie usuniete
for p=1:1:fs/d*t
    FFT = abs(fft(y((p-1)*d+1:p*d)));
    FFT = FFT(1:d/2);
    rms(1) = sqrt(sum(FFT(1:2).^2));
    rms(2) = sqrt(sum(FFT(3:4).^2));
    rms(3) = sqrt(sum(FFT(5:8).^2));
    rms(4) = sqrt(sum(FFT(9:16).^2));
    rms(5) = sqrt(sum(FFT(17:32).^2));
    rms(6) = sqrt(sum(FFT(33:64).^2));
    rms(7) = sqrt(sum(FFT(65:128).^2));
    rms(8) = sqrt(sum(FFT(129:256).^2));
    rms(9) = sqrt(sum(FFT(257:441).^2));
    rms = (rms/d).*sqrt(2);         %zgodnie z twierdzeniem Parseval'a

     RMS = [RMS ; rms];
end
RMS = (RMS(2:end, :))';         %usuniecie zbednych wartosci (wiersz 20)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%prawdopodobne bpm dla 9 zakresow
BPM(1:200,1:9) = 0;             
for bpm = 60:200
    for freq = 1:9
        WAR = [];
        for kr = 1:floor(60*cz/bpm)-1
            war = 0;
            q = 0;
            while floor(kr)<cz*t+1
                war = war + (RMS(freq,floor(kr)));
                q = q+1;
                kr = kr + 60*cz/bpm;
            end
            WAR = [WAR war/q];          
        end
        BPM(bpm,freq) = max(WAR);       %wybieranie maksymalnej wartosci dla srednich policzonych z kolejnych przesuni��
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%wyzerowanie zbednych wartosci bpm (wybieramy jedynie najbardziej
%narzucajace sie tempa w pasmach)
for freq =1:9                                                   
    for k = 60:200                                            
        if BPM(k,freq)<0.9*max(BPM(:,freq))
             BPM(k,freq) = 0;
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%tworzenie koncowego wykresu i wybieranie konkretnej wartosci
BP = sum(BPM');
figure(2)
bar(BP);
title(['Najbardziej prawdopodobne tempa dla ' file ]);
xlabel('BPM');
grid on;
disp(['tempo wynosi: ' num2str(find(BP == max(BP)))])           %find(BP == max(BP)) tp indeks najwiekszej wartosci (bpm)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%szukanie drugiego z kolei bpm
if (find(BP == max(BP))==60)
    indx = 61;
else
    indx = 60;
end
indx = 60;
for k= 61:200                      
    if BP(k)>BP(indx) & BP(k)~=max(BP);
        indx = k;
    end
end
disp(['(ewentualnie: ' num2str(indx) ')'])